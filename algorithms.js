//Big O notation always is the worst case
const unsortedTable=[100,4,60,7,2,9,10,40,160,200, 7, 0]
const search=60
let total=0

//Selection sort O(n^2)
//Find min from i to the end of array and swap min with element on i-position
const selectionSort=function(table){
    let temp=0
    for(let i=0; i<table.length; i++){
       let min=i //at first i is new minimum
       for(let j=i; j<table.length; j++){ 
            if(table[j]<table[min]){
                min=j //but it can change to j if it's smaller
            }
       }
       if(i!==min){ //swap i(now bigger) with min(smmaler and it's actually smallest from j)
           temp=table[i]
           table[i]=table[min]
           table[min]=temp
       }
    }
}

const quickSort=function(table){
    let first=0
    let last=table.length-1
        if(table.length<2){
            return table
        }else{
            let pivot=Math.floor((first+last)/2)
            let less=[]
            let greater=[]
            for(let i=0; i<pivot.indexOf();i++){
                less.push(table[i])
            }
            for(let i=table.length; i>pivot.indexOf();i--){
                greater.push(table[i])
            }           
            return quickSort(less)+[pivot]+quickSort(greater)
        }
}

//Linear search O(n)
//Check every element
const linearSearch=function(table, x){
    return found=table.indexOf(x)
}

//Binary search O(log n)
//Divide array in half until you find x
const binarySearch=function(table, x){
    let first=0
    let last=table.length-1
    while(first<=last){
        let middle=Math.floor((first+last)/2)
        if(table[middle]===x){
            return middle 
        }else if(table[middle]>x){
            last=middle-1
        }else{
            first=middle+1
        }
    }
    return null
}

//Factorial-recursion
//Recursion uses call stack/execution stack when it calls subroutine and return
const factorial=function(x){
    if(x===1){ //base case
        return 1
    }else{ //recursion step
        return x*factorial(x-1)
    }
}

//Add up-recursion
const addUp=function(table){
    if(table.length===0){ //check if array is empty, table===[] is bad, because [] makes new object and they are note the same object
        return 0
    }else{
        return table[0]+addUp(table.splice(1))
    }
}

//selectionSort(unsortedTable)
quickSort(unsortedTable)
console.log(unsortedTable)
console.log(linearSearch(unsortedTable, search))
console.log(binarySearch(unsortedTable, search))
console.log(factorial(search))
console.log(addUp(unsortedTable))
